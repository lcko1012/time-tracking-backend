package com.example.timetrackingbackend.sprint.service;

import com.example.timetrackingbackend.sprint.model.Sprint;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SprintService {
    public List<Sprint> getActiveSprintList();

    public Sprint saveSprint(Sprint sprint);

    public Sprint getSprintById(String id);

    public void deleteSprint(String id);

    public List<Sprint> getActiveSprintListByCurrentDate();

}
