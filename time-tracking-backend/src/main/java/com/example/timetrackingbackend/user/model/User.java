package com.example.timetrackingbackend.user.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "users")
@NoArgsConstructor
@Data
public class User {
    @Id
    private String id;
    @Field(name="username")
    private String username;
    private String refName;
    private String password;

    public User(String username, String refName, String password){
        this.username = username;
        this. refName = refName;
        this.password = password;
    }
}
