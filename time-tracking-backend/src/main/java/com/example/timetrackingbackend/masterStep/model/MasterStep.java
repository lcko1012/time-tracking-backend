package com.example.timetrackingbackend.masterStep.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "master_steps")
public class MasterStep {
    private String id;
    private String name;
    private Boolean active;
}
