package com.example.timetrackingbackend.step.service;

import com.example.timetrackingbackend.step.model.Step;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StepService {
    public List<Step> getStepList();
    public List<Step> getStepListByUserStoryId(String id);
    public Step saveStep(Step step);
    public void saveAllStep(List<Step> stepList);
    public Step findStepById(String id);
    public List<Step> getStepListByUserStoryIdIn(List<String> userStoryIdList);
}
