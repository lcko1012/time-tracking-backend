package com.example.timetrackingbackend.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandleAdvice {
    @ExceptionHandler(CustomException.class)
    public ResponseEntity handleException(CustomException exception){
        return ResponseEntity.status(exception.getHttpStatus()).body(exception.getMessage());
    }
}
