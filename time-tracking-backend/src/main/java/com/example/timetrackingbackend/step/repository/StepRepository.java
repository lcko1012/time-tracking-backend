package com.example.timetrackingbackend.step.repository;

import com.example.timetrackingbackend.step.model.Step;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface StepRepository extends MongoRepository<Step, String> {

    List<Step> findStepByUserStoryId(String id);
    Step findStepById(String id);
    List<Step> findStepsByUserStoryIdIn(List<String> userStoryIdList);
}
