package com.example.timetrackingbackend.step.service;

import com.example.timetrackingbackend.step.model.Step;
import com.example.timetrackingbackend.step.repository.StepRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class StepServiceImpl implements StepService {
    private final StepRepository stepRepository;

    @Override
    public List<Step> getStepList() {
        List<Step> stepList = stepRepository.findAll();
        return stepList;
    }

    @Override
    public List<Step> getStepListByUserStoryId(String id) {
        List<Step> stepListByUserStoryId = stepRepository.findStepByUserStoryId(id);
        return stepListByUserStoryId;
    }

    @Override
    public Step saveStep(Step step) {
        Step savedStep = stepRepository.save(step);
        return savedStep;
    }

    @Override
    public void saveAllStep(List<Step> stepList) {
        stepRepository.saveAll(stepList);
    }

    @Override
    public Step findStepById(String id) {
        Step existedStep = stepRepository.findStepById(id);
        return existedStep;
    }

    @Override
    public List<Step> getStepListByUserStoryIdIn(List<String> userStoryIdList) {
        return stepRepository.findStepsByUserStoryIdIn(userStoryIdList);
    }
}
