package com.example.timetrackingbackend.user.controller;

import com.example.timetrackingbackend.authentication.model.AuthenticationResponse;
import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.security.jwt.JwtTokenService;
import com.example.timetrackingbackend.user.model.User;
import com.example.timetrackingbackend.user.model.UserDTO;
import com.example.timetrackingbackend.user.service.UserService;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenService jwtTokenService;

    @PostMapping("/register")
    public ResponseEntity<User> createUser(@RequestBody User user) throws Exception, CustomException {
        User newUser = userService.createUser(user);
        return ResponseEntity.ok(newUser);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> loginUser(@RequestBody User user) {
        UserDTO loginUser = userService.login(user);
        String username = user.getUsername();
        String password = user.getPassword();
        String jwtToken = "";
        try {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            jwtToken = jwtTokenService.createJwtToken(authentication, username);

        } catch (Exception e) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Token error");
        }
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(loginUser, jwtToken);
        return ResponseEntity.ok(authenticationResponse);
    }
}
