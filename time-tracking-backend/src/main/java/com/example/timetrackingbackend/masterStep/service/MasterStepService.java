package com.example.timetrackingbackend.masterStep.service;

import com.example.timetrackingbackend.masterStep.model.MasterStep;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MasterStepService {
    public List<MasterStep> getActiveMasterStepList();
    public MasterStep saveMasterStep(MasterStep masterStep);
    public void saveALlMasterStep(List<MasterStep> masterStepList);
    public Boolean deleteMasterStep(String id);
    public MasterStep findMasterStepById(String id);
    public Boolean findMasterStepByName(String name);
}
