package com.example.timetrackingbackend.user.service;

import com.example.timetrackingbackend.user.model.User;
import com.example.timetrackingbackend.user.model.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {
    public User createUser(User user);
    public UserDTO login(User user);
}
