package com.example.timetrackingbackend.masterStep.service;

import com.example.timetrackingbackend.masterStep.model.MasterStep;
import com.example.timetrackingbackend.masterStep.repository.MasterStepRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MasterStepServiceImpl implements MasterStepService {

    private final MasterStepRepository masterStepRepository;

    @Override
    public List<MasterStep> getActiveMasterStepList() {
        List<MasterStep> masterStepList = masterStepRepository.findAllByActiveIsTrue();
        return masterStepList;
    }

    @Override
    public MasterStep saveMasterStep(MasterStep masterStep) {
        masterStep.setActive(true);
        MasterStep returnMasterStep = masterStepRepository.save(masterStep);
        return returnMasterStep;
    }

    @Override
    public void saveALlMasterStep(List<MasterStep> masterStepList) {
        masterStepRepository.saveAll(masterStepList);
    }

    @Override
    public Boolean deleteMasterStep(String id) {
        MasterStep deletedMasterStep = masterStepRepository.findMasterStepById(id);
        deletedMasterStep.setActive(false);
        masterStepRepository.save(deletedMasterStep);
        return true;
    }

    @Override
    public MasterStep findMasterStepById(String id) {
        MasterStep existedMasterStep = masterStepRepository.findMasterStepById(id);
        return existedMasterStep;
    }

    @Override
    public Boolean findMasterStepByName(String name) {
        MasterStep existedMasterStepName = masterStepRepository.findMasterStepByNameAndActiveIsTrue(name);
        if(existedMasterStepName != null) return true;
        return false;
    }
}
