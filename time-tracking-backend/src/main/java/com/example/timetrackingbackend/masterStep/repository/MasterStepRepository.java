package com.example.timetrackingbackend.masterStep.repository;

import com.example.timetrackingbackend.masterStep.model.MasterStep;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MasterStepRepository extends MongoRepository<MasterStep, String> {

    MasterStep findMasterStepByNameAndActiveIsTrue(String name);

    MasterStep findMasterStepById(String id);

    List<MasterStep> findAllByActiveIsTrue();
}
