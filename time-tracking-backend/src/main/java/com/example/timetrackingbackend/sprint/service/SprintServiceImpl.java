package com.example.timetrackingbackend.sprint.service;

import com.example.timetrackingbackend.sprint.model.Sprint;
import com.example.timetrackingbackend.sprint.repository.SprintRepository;
import com.example.timetrackingbackend.sprint.service.SprintService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class SprintServiceImpl implements SprintService {
    private final SprintRepository sprintRepository;

    @Override
    public List<Sprint> getActiveSprintList() {
        return sprintRepository.findSprintByActiveIsTrue();
    }

    @Override
    public Sprint saveSprint(Sprint sprint) {
        sprint.setActive(true);
        return sprintRepository.save(sprint);
    }

    @Override
    public Sprint getSprintById(String id){
        return sprintRepository.findSprintById(id);
    }

    @Override
    public void deleteSprint(String id){
        Sprint deletedSprint = sprintRepository.findSprintById(id);
        deletedSprint.setActive(false);
        sprintRepository.save(deletedSprint);
    }

    @Override
    public List<Sprint> getActiveSprintListByCurrentDate() {
        Long currentDateUnix = System.currentTimeMillis();
        List<Sprint> sprintList = sprintRepository.findSprintByActiveIsTrueAndStartDateUnixIsLessThanEqualAndEndDateUnixGreaterThanEqual(currentDateUnix, currentDateUnix);
        return sprintList;
    }
}
