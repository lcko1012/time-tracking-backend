package com.example.timetrackingbackend.userStory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "user_stories")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserStory {
    @Id
    private String id;
    @Field(name="name")
    private String name;
    private Integer point;
    private String sprintId;
    private Boolean active;
    private Integer totalTime;
}