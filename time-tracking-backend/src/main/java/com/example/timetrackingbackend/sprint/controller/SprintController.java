package com.example.timetrackingbackend.sprint.controller;

import com.example.timetrackingbackend.sprint.model.Sprint;
import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.sprint.service.SprintService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/sprint")
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class SprintController {
    private final SprintService sprintService;

    @GetMapping
    public ResponseEntity<List<Sprint>> getActiveSprintList() {
        return ResponseEntity.ok(sprintService.getActiveSprintList());
    }

    @PostMapping
    public ResponseEntity<Sprint> createSprint(@RequestBody Sprint sprint) {
        return new ResponseEntity<Sprint>(sprintService.saveSprint(sprint), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Sprint> updateSprint(@RequestBody Sprint sprint, @PathVariable String id){
        if(!id.equals(sprint.getId())){
            throw new CustomException(HttpStatus.BAD_REQUEST, id);
        }
        else {
            Sprint updatedSprint = sprintService.getSprintById(id);
            if(updatedSprint == null){
                throw new CustomException(HttpStatus.BAD_REQUEST, "Sprint doesn't exist");
            }
            else {
                return new ResponseEntity<Sprint>(sprintService.saveSprint(sprint), HttpStatus.OK);
            }
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteSprint(@PathVariable String id){
        if(sprintService.getSprintById(id) == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Sprint doesn't exist");
        }
        else {
            sprintService.deleteSprint(id);
            return ResponseEntity.ok(HttpStatus.OK);
        }
    }
}
