package com.example.timetrackingbackend.masterStep.controller;

import com.example.timetrackingbackend.masterStep.model.MasterStep;
import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.masterStep.service.MasterStepService;
import com.example.timetrackingbackend.sprint.model.Sprint;
import com.example.timetrackingbackend.sprint.service.SprintService;
import com.example.timetrackingbackend.step.model.Step;
import com.example.timetrackingbackend.step.service.StepService;
import com.example.timetrackingbackend.userStory.model.UserStory;
import com.example.timetrackingbackend.userStory.service.UserStoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*")
@RequestMapping("/master-step")
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class MasterStepController {
    private final MasterStepService masterStepService;
    private final SprintService sprintService;
    private final UserStoryService userStoryService;
    private final StepService stepService;

    @GetMapping("")
    public ResponseEntity<List<MasterStep>> getMasterStepList() {
        List<MasterStep> masterStepList = masterStepService.getActiveMasterStepList();
        return ResponseEntity.ok(masterStepList);
    }

    @PostMapping("")
    public ResponseEntity<MasterStep> createMasterStep(@RequestBody MasterStep masterStep) {
        Boolean isExistedMasterStepName = masterStepService.findMasterStepByName(masterStep.getName());

        List<Sprint> activeSprintList = sprintService.getActiveSprintListByCurrentDate();
        List<String> activeSprintIdList = activeSprintList.stream().map(Sprint::getId).collect(Collectors.toList());
        List<UserStory> userStoryList = userStoryService.getUserStoryBySprintIdIn(activeSprintIdList);
        List<String> userStoryIdList = userStoryList.stream().map(UserStory::getId).collect(Collectors.toList());
        List<Step> stepList = stepService.getStepListByUserStoryIdIn(userStoryIdList);
        List<Step> newStepList = new ArrayList<>();

        if(isExistedMasterStepName) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Name of step existed");
        }
        else {
            MasterStep newMasterStep = masterStepService.saveMasterStep(masterStep);

            if(stepList.size() > 0) {
                for (Step step : stepList) {
                    if(step.getMasterStepName().equals(masterStep.getName())) {
                        userStoryIdList.remove(step.getUserStoryId());
                    }
                }
                for (String userStoryId : userStoryIdList) {
                        Step newStep = new Step();
                        newStep.setUserStoryId(userStoryId);
                        newStep.setMasterStepId(newMasterStep.getId());
                        newStep.setMasterStepName(newMasterStep.getName());
                        newStep.setTimeInSecond(0);
                        newStepList.add(newStep);
                }
                stepService.saveAllStep(newStepList);
            }

            return ResponseEntity.ok(newMasterStep);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<MasterStep> updateMasterStep(@PathVariable String id , @RequestBody MasterStep masterStep) {
        MasterStep existedMasterStep = masterStepService.findMasterStepById(id);
        Boolean isExistedMasterStepName = masterStepService.findMasterStepByName(masterStep.getName());
        if(!masterStep.getId().equals(id)){
            throw new CustomException(HttpStatus.BAD_REQUEST, "Id does not match");
        } else if(existedMasterStep == null ) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Step does not exist");
        } else if(isExistedMasterStepName) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Name of step existed");
        }
        else {
            MasterStep updateMasterStep = masterStepService.saveMasterStep(masterStep);
            return ResponseEntity.ok(updateMasterStep);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMasterStep(@PathVariable String id) {
        MasterStep existedMasterStep = masterStepService.findMasterStepById(id);
        if(existedMasterStep == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Step does not exist");
        }
        else if(masterStepService.deleteMasterStep(id)) {
            return ResponseEntity.ok("Delete " + id + " successfully");
        }
        else throw new CustomException(HttpStatus.BAD_REQUEST, "Failed to delete " + id);
    }
}
