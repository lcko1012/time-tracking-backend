package com.example.timetrackingbackend.step.controller;

import com.example.timetrackingbackend.step.model.Step;
import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.step.service.StepService;
import com.example.timetrackingbackend.userStory.model.UserStory;
import com.example.timetrackingbackend.userStory.service.UserStoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*")
@RequestMapping("/step")
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class StepController {

    private final StepService stepService;
    private final UserStoryService userStoryService;

    @GetMapping("")
    public ResponseEntity<List<Step>> getStepList() {
        List<Step> stepList = stepService.getStepList();
        return ResponseEntity.ok(stepList);
    }

    @GetMapping("/by-user-story/{user-story-id}")
    public ResponseEntity<List<Step>> getStepListByUserStoryId(@PathVariable(name = "user-story-id") String userStoryId) {
        List<Step> stepListByUserStoryId = stepService.getStepListByUserStoryId(userStoryId);
        return ResponseEntity.ok(stepListByUserStoryId);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Step> updateStep(@PathVariable String id, @RequestBody Step step) {
        Step existedStep = stepService.findStepById(id);
        if (!step.getId().equals(id)) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Id does not match");
        }
        else if(existedStep == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Step does not exist");
        }
        else {
         Step updateStep = stepService.saveStep(step);
         List<Step> stepListByUserStory = stepService.getStepListByUserStoryId(step.getUserStoryId());
         int totalTime = stepListByUserStory.stream().mapToInt(stepItem -> stepItem.getTimeInSecond()).sum();
         UserStory userStory = userStoryService.getUserStoryById(step.getUserStoryId());
         userStory.setTotalTime(totalTime);
         userStoryService.saveUserStory(userStory);
         return ResponseEntity.ok(updateStep);
        }
    }
}

