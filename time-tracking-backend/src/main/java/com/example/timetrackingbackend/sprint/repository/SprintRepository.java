package com.example.timetrackingbackend.sprint.repository;

import com.example.timetrackingbackend.sprint.model.Sprint;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SprintRepository extends MongoRepository<Sprint, String> {
    Sprint findSprintById(String id);
    List<Sprint> findSprintByActiveIsTrue();
    List<Sprint> findSprintByActiveIsTrueAndStartDateUnixIsLessThanEqualAndEndDateUnixGreaterThanEqual(Long startDateUnix, Long endDateUnix);
}
