package com.example.timetrackingbackend.userStory.service;

import com.example.timetrackingbackend.userStory.model.UserStory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserStoryService {
    public UserStory saveUserStory(UserStory userStory);

    public List<UserStory> getUserStoryBySprintId(String sprintId);

    public UserStory getUserStoryById(String id);

    public UserStory deleteUserStory(String id);

    public List<UserStory> getUserStoryBySprintIdIn(List<String> sprintIdList);
}
