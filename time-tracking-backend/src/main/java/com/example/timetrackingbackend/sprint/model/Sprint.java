package com.example.timetrackingbackend.sprint.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;


@Component
@Document(collection = "sprints")
@NoArgsConstructor
@Data
public class Sprint {
    @Id
    private String id;
    private String name;
    private String startDate;
    private String endDate;
    private Long startDateUnix;
    private Long endDateUnix;
    private boolean active;
}
