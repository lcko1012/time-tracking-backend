package com.example.timetrackingbackend.userStory.service;

import com.example.timetrackingbackend.userStory.model.UserStory;
import com.example.timetrackingbackend.userStory.repository.UserStoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UserStoryServiceImpl implements UserStoryService {

    private final UserStoryRepository userStoryRepository;

    @Override
    public UserStory saveUserStory(UserStory userStory) {
        userStory.setActive(true);
        return userStoryRepository.save(userStory);
    }

    @Override
    public List<UserStory> getUserStoryBySprintId(String sprintId) {
        return userStoryRepository.findUserStoryBySprintIdAndActiveIsTrue(sprintId);
    }

    @Override
    public UserStory getUserStoryById(String id) {
        return userStoryRepository.findUserStoryById(id);
    }

    @Override
    public UserStory deleteUserStory(String id) {
        UserStory inActiveUserStory = userStoryRepository.findUserStoryById(id);
        inActiveUserStory.setActive(false);
        return userStoryRepository.save(inActiveUserStory);
    }

    @Override
    public List<UserStory> getUserStoryBySprintIdIn(List<String> sprintIdList) {
        return userStoryRepository.findUserStoryBySprintIdIn(sprintIdList);
    }
}