package com.example.timetrackingbackend.userStory.repository;

import com.example.timetrackingbackend.userStory.model.UserStory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserStoryRepository extends MongoRepository<UserStory, String> {
    UserStory findUserStoryById(String id);
    List<UserStory> findUserStoryBySprintIdAndActiveIsTrue(String id);
    List<UserStory> findUserStoryBySprintIdIn(List<String> sprintIdList);
}
