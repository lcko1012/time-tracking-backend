package com.example.timetrackingbackend.user.repository;

import com.example.timetrackingbackend.user.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    User findUserByUsername(String username);
}

