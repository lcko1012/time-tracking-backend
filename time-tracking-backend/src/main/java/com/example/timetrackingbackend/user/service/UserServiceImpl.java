package com.example.timetrackingbackend.user.service;

import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.user.model.User;
import com.example.timetrackingbackend.user.model.UserDTO;
import com.example.timetrackingbackend.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@RequiredArgsConstructor
@Component
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User existedUser = userRepository.findUserByUsername(username);
        return new org.springframework.security.core.userdetails.User(
                existedUser.getUsername(),
                existedUser.getPassword(),
                new ArrayList<>()
        );
    }

    @Override
    public User createUser(User user) {
        User existedUser = userRepository.findUserByUsername(user.getUsername());
        if(existedUser != null) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Account already exists");
        }
        else {
            User createdUser = userRepository.save(user);
            return createdUser;
        }
    }

    @Override
    public UserDTO login(User user) {
        User foundUser = userRepository.findUserByUsername(user.getUsername());
        if(foundUser == null) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "Account does not exist");
        }
        else {
            if(foundUser.getPassword().equals(user.getPassword())) {
                return new UserDTO().toUserDTO(foundUser);
            }
            else {
                throw new CustomException(HttpStatus.BAD_REQUEST, "Wrong password");
            }
        }
    }
}
