package com.example.timetrackingbackend.userStory.controller;

import com.example.timetrackingbackend.masterStep.model.MasterStep;
import com.example.timetrackingbackend.masterStep.service.MasterStepService;
import com.example.timetrackingbackend.step.model.Step;
import com.example.timetrackingbackend.step.service.StepService;
import com.example.timetrackingbackend.userStory.model.UserStory;
import com.example.timetrackingbackend.exception.CustomException;
import com.example.timetrackingbackend.userStory.service.UserStoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/user-story")
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class UserStoryController {
    private final UserStoryService userStoryService;
    private final MasterStepService masterStepService;
    private final StepService stepService;

    @PostMapping
    public ResponseEntity<UserStory> createUserStory(@RequestBody UserStory userStory) {
        UserStory newUserStory = userStoryService.saveUserStory(userStory);
        List<MasterStep> masterStepList = masterStepService.getActiveMasterStepList();
        List<Step> newStepList = new ArrayList<>();
        for (MasterStep masterStep : masterStepList) {
            Step newStep = new Step();
            newStep.setMasterStepId(masterStep.getId());
            newStep.setMasterStepName(masterStep.getName());
            newStep.setUserStoryId(newUserStory.getId());
            newStep.setTimeInSecond(0);
            newStepList.add(newStep);
        }
        stepService.saveAllStep(newStepList);
        return new ResponseEntity<>(newUserStory, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserStory> getUserStoryById(@PathVariable String id) {
        UserStory userStory = userStoryService.getUserStoryById(id);
        return ResponseEntity.ok(userStory);
    }

    @GetMapping("/list/{sprint-id}")
    public ResponseEntity<List<UserStory>> getUserStoryBySpringId(@PathVariable(name = "sprint-id") String sprintId) {
        List<UserStory> userStoryList = userStoryService.getUserStoryBySprintId(sprintId);
        return ResponseEntity.ok(userStoryList);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserStory> updateUserStory(@RequestBody UserStory userStory, @PathVariable String id) {
        if (!id.equals(userStory.getId())){
            throw new CustomException(HttpStatus.BAD_REQUEST, id);
        }
        else {
            UserStory editedUserStory = userStoryService.saveUserStory(userStory);
            return ResponseEntity.ok(editedUserStory);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteUserStory(@PathVariable String id){
        userStoryService.deleteUserStory(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}