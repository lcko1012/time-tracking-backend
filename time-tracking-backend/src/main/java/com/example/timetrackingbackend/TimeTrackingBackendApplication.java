package com.example.timetrackingbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeTrackingBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeTrackingBackendApplication.class, args);
	}

}
