package com.example.timetrackingbackend.step.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "steps")
public class Step {
    private String id;
    private String userStoryId;
    private String masterStepId;
    private String masterStepName;
    private Integer timeInSecond;
}
